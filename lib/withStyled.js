"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = withStyled;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function withStyled(COMP) {
  var styledComponent = (0, _styledComponents.withTheme)(function (props) {
    return /*#__PURE__*/_react.default.createElement(COMP, props);
  });
  styledComponent.defaultProps = {
    theme: {}
  };
  return (0, _styledComponents.default)(styledComponent);
}
//# sourceMappingURL=withStyled.js.map