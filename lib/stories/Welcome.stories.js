"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toStorybook = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _addonLinks = require("@storybook/addon-links");

var _demo = require("@storybook/react/demo");

var _default = {
  title: 'Welcome',
  component: _demo.Welcome
};
exports.default = _default;

var toStorybook = function toStorybook() {
  return /*#__PURE__*/_react.default.createElement(_demo.Welcome, {
    showApp: (0, _addonLinks.linkTo)('Button')
  });
};

exports.toStorybook = toStorybook;
toStorybook.story = {
  name: 'to Storybook'
};
//# sourceMappingURL=Welcome.stories.js.map