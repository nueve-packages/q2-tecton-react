/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, { ReactNode, MutableRefObject } from 'react';
export declare type ButtonMouseEvent = React.MouseEvent<Q2Btn, MouseEvent>;
export declare type ButtonFocusEvent = React.FocusEvent<Q2Btn>;
export interface IntrinsicHTMLElement extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    ref?: any;
}
export interface Q2Btn extends Omit<IntrinsicHTMLElement, 'onFocus' | 'onMouseDown' | 'onMouseUp' | 'onBlur' | 'onClick'> {
    active?: boolean;
    ariaLabel?: string;
    badge?: boolean;
    block?: boolean;
    disabled?: boolean;
    icon?: boolean;
    onFocus?: (event: ButtonFocusEvent) => void;
    onMouseDown?: (event: ButtonMouseEvent) => void;
    onMouseUp?: (event: ButtonMouseEvent) => void;
    onBlur?: (e: ButtonFocusEvent) => void;
    onClick?: (e: ButtonMouseEvent) => void;
    styles?: HashMap;
}
export declare type CalendarChangeEvent = React.ChangeEvent<Q2Calendar>;
export declare type CalendarMouseEvent = React.MouseEvent<Q2Calendar, MouseEvent>;
export interface Q2Calendar extends Omit<IntrinsicHTMLElement, 'onChange' | 'onMouseDown' | 'onMouseUp' | 'onBlur' | 'onClick'> {
    addEventListener?: any;
    onChange?: (event: CalendarChangeEvent) => void;
    calendarLabel?: string;
    cutoffTime?: string;
    daysOfWeekChecksum?: number;
    disabled?: boolean;
    disabledMsg?: string;
    disclaimer?: string;
    displayFormat?: string;
    endDate?: string;
    errors?: string[];
    invalidDates?: string[];
    label?: string;
    leftAlign?: boolean;
    optional?: boolean;
    removeEventListener?: any;
    startDate?: string;
    styles?: HashMap;
    validDates?: string[];
    value?: string;
}
export declare type SectionChangeEvent = React.ChangeEvent<Q2Section>;
export interface Q2Section extends Omit<IntrinsicHTMLElement, 'onChange'> {
    children?: ReactNode | string;
    label?: string;
    collapsible?: boolean;
    expanded?: boolean;
}
export declare type DropdownMouseEvent = React.MouseEvent<Q2Select, MouseEvent>;
export declare type DropdownChangeEvent = React.ChangeEvent<Q2Select>;
export declare type DropdownInputEvent = React.SyntheticEvent<Q2Select>;
export interface Q2Select extends Omit<IntrinsicHTMLElement, 'onChange' | 'onClick' | 'onInput'> {
    children: ReactNode;
    errors?: string[];
    label?: string;
    multilineOptions?: boolean;
    multiple?: boolean;
    optional?: boolean;
    options?: HashMap;
    searchable?: boolean;
    selectedOptions?: string[];
}
export interface Q2Option extends IntrinsicHTMLElement {
    value: string;
    display: string;
    disabled?: boolean;
    children: ReactNode | string;
    active?: boolean;
    selected?: boolean;
    class?: string;
}
export declare type CheckboxChangeEvent = React.ChangeEvent<Q2CheckBox>;
export interface Q2CheckBox extends Omit<IntrinsicHTMLElement, 'onChange'> {
    red?: MutableRefObject<Q2CheckBox | undefined>;
    addEventListener?: any;
    checked?: boolean;
    disabled?: boolean;
    label?: string;
    removeEventListener?: any;
    type?: string;
    value?: string;
}
export declare type InputChangeEvent = React.FormEvent<Q2Input>;
export declare type InputSyntheticEvent = React.SyntheticEvent<Q2Input>;
export interface Q2Input extends IntrinsicHTMLElement {
    addEventListener?: any;
    ariaLabel?: string;
    clearable?: boolean;
    disabled?: boolean;
    errors?: string[];
    formatModifier?: string;
    hideMessages?: boolean;
    hints?: string[];
    iconLeft?: string;
    iconRight?: string;
    label?: string;
    maxlength?: string;
    name?: string;
    optional?: boolean;
    removeEventListener?: any;
    role?: string;
    type?: string;
    value?: string;
    className?: string;
    class?: string;
}
export interface Q2Message extends IntrinsicHTMLElement {
    children?: ReactNode;
    description?: boolean;
    type?: string;
}
export interface Q2Icon extends IntrinsicHTMLElement {
    type?: string;
    styles?: HashMap;
}
export interface Q2Loading extends IntrinsicHTMLElement {
    class?: string;
    inline?: boolean;
}
export interface Q2OptionGroup extends IntrinsicHTMLElement {
    class?: string;
    label: string;
    disabled?: boolean;
    children: ReactNode | string;
}
export interface Q2DropDown extends IntrinsicHTMLElement {
    type?: string;
    icon?: string;
    label?: string;
    disabled?: boolean;
    children?: ReactNode | string;
}
export interface Q2DropdownItem extends IntrinsicHTMLElement {
    disabled?: boolean;
    value?: string;
    label?: string;
    removable?: boolean;
    separator?: boolean;
    children?: ReactNode | string;
}
export interface Q2DropdownOptionGroup extends IntrinsicHTMLElement {
    label?: string;
    children?: ReactNode | string;
}
export declare type RadioButtonChangeEvent = React.ChangeEvent<Q2Radio>;
export declare type RadioButtonMouseEvent = React.MouseEvent<Q2Radio, MouseEvent>;
export declare type RadioButtonFormEvent = React.FormEvent<Q2Radio>;
export interface Q2Radio extends Omit<IntrinsicHTMLElement, 'onFocus' | 'onBlur' | 'onClick' | 'onChange'> {
    label?: string;
    disabled?: boolean;
    value?: string;
    ariaLabel?: string;
}
export declare type RadioChangeEvent = React.ChangeEvent<Q2RadioGroup>;
export interface Q2RadioGroup extends Omit<IntrinsicHTMLElement, 'onFocus' | 'onBlur' | 'onClick'> {
    label?: string;
    disabled?: boolean;
    value?: string;
    name?: string;
    children?: ReactNode | string;
}
export declare type InlineChangeEvent = React.ChangeEvent<Q2EditableField>;
export interface Q2EditableField extends IntrinsicHTMLElement {
    addEventListener?: any;
    ariaLabel?: string;
    editing?: boolean;
    errors?: string[];
    formatModifier?: string;
    hints?: string[];
    label?: string;
    maxlength?: number;
    persistentLabel?: boolean;
    removeEventListener?: any;
    type?: string;
    value?: string;
}
export declare type TabsChangeEvent = React.ChangeEvent<Q2TabContainer>;
export interface Q2TabContainer extends Omit<IntrinsicHTMLElement, 'onChange'> {
    children?: ReactNode;
    name?: string;
    type?: string;
    value?: string;
}
export interface Q2TabPane extends IntrinsicHTMLElement {
    value?: string;
    label?: string;
    name?: string;
    children?: ReactNode | string;
}
declare global {
    namespace JSX {
        interface IntrinsicElements {
            'q2-btn': Q2Btn;
            'q2-calendar': Q2Calendar;
            'q2-checkbox': Q2CheckBox;
            'q2-dropdown': Q2DropDown;
            'q2-dropdown-item': Q2DropdownItem;
            'q2-dropdown-option-group': Q2DropdownOptionGroup;
            'q2-editable-field': Q2EditableField;
            'q2-icon': Q2Icon;
            'q2-input': Q2Input;
            'q2-loading': Q2Loading;
            'q2-message': Q2Message;
            'q2-optgroup': Q2OptionGroup;
            'q2-option': Q2Option;
            'q2-radio': Q2Radio;
            'q2-radio-group': Q2RadioGroup;
            'q2-section': Q2Section;
            'q2-select': Q2Select;
            'q2-tab-container': Q2TabContainer;
            'q2-tab-pane': Q2TabPane;
        }
    }
}
export interface HashMap<T = any> {
    [key: string]: T;
}
