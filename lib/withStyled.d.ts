import React, { ComponentType } from 'react';
export default function withStyled(COMP: ComponentType<any>): import("styled-components").ThemedStyledFunction<React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & {
    theme?: any;
}>, any, {}, never>;
