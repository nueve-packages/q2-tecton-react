"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _presets = require("@theme-ui/presets");

var _lodash = _interopRequireDefault(require("lodash.merge"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var theme = _objectSpread(_objectSpread({}, _presets.base), {}, {
  colors: _objectSpread(_objectSpread({}, _presets.base.colors), {}, {
    a11yGrayColor: 'var(--t-a11y-gray-color)',
    stopLightColor: 'var(--const-stoplight-alert)',
    buttonDefaultBg: 'var(--t-button-default-bg)',
    buttonDefaultFontColor: 'var(--t-button-default-font-color)',
    buttonDefaultHoverBg: 'var(--t-button-default-hover-bg)',
    buttonDefaultHoverFontColor: 'var(--t-button-default-hover-font-color)',
    buttonPrimaryBg: 'var(--t-button-primary-bg)',
    buttonPrimaryFontColor: 'var(--t-button-primary-font-color)',
    buttonPrimaryHoverBg: 'var(--t-button-primary-hover-bg)',
    buttonPrimaryHoverFontColor: 'var(--t-button-primary-hover-font-color)',
    checkboxFill: 'var(--t-checkbox-fill)',
    checkboxText: 'var(--t-checkbox-text)',
    contentBg: 'var(--t-content-bg)',
    contentBorderColor: 'var(--t-content-border-color)',
    contentFontColor: 'var(--t-content-font-color)',
    fontColor: 'var(--t-font-color)',
    highlight1: 'var(--t-highlight-1)',
    highlight2: 'var(--t-highlight-2)',
    highlight3: 'var(--t-highlight-3)',
    highlight4: 'var(--t-highlight-4)',
    highlight5: 'var(--t-highlight-5)',
    highlight6: 'var(--t-highlight-6)',
    highlight7: 'var(--t-highlight-7)',
    highlight8: 'var(--t-highlight-8)',
    highlight9: 'var(--t-highlight-9)',
    highlight10: 'var(--t-highlight-10)',
    highlight11: 'var(--t-highlight-11)',
    highlight12: 'var(--t-highlight-12)',
    highlight13: 'var(--t-highlight-13)',
    highlight14: 'var(--t-highlight-14)',
    highlight15: 'var(--t-highlight-15)',
    iconCap: 'var(--t-icon-cap)',
    iconFill: 'var(--t-icon-fill)',
    iconStrokePrimary: 'var(--t-icon-stroke-primary)',
    iconStrokeSecondary: 'var(--t-icon-stroke-secondary)',
    iconStrokeWidth: 'var(--t-icon-stroke-width)',
    inputBg: 'var(--t-input-bg)',
    inputBorder: '1px solid var(--t-input-border)',
    inputErrorFontColor: 'var(--t-input-error-font-color)',
    inputError: 'var(--t-input-error)',
    inputFocusFontColor: 'var(--t-input-focus-font-color)',
    inputFocus: 'var(--t-input-focus)',
    inputFontColor: 'var(--t-input-font-color)',
    inputOutline: 'var(--t-input-outline)',
    inputPlaceholderFontColor: 'var(--t-input-placeholder-font-color)',
    inputSuccessFontColor: 'var(--t-input-success-font-color)',
    inputSuccess: 'var(--t-input-success)',
    inputWarningFontColor: 'var(--t-input-warning-font-color)',
    inputWarning: 'var(--t-input-warning)',
    linkColor: 'var(--t-link-color)',
    linkHoverColor: 'var(--t-link-hover-color)',
    loadingPrimaryColor: 'var(--t-loading-primary-color)',
    loadingSecondaryColor: 'var(--t-loading-secondary-color)',
    primary: 'var(--t-primary)',
    primaryD1: 'var(--t-primary-d1)',
    primaryD2: 'var(--t-primary-d2)',
    primaryL1: 'var(--t-primary-l1)',
    primaryL2: 'var(--t-primary-l2)',
    secondary: 'var(--t-secondary)',
    secondaryD1: 'var(--t-secondary-d1)',
    secondaryD2: 'var(--t-secondary-d2)',
    secondaryL1: 'var(--t-secondary-l1)',
    secondaryL2: 'var(--t-secondary-l2)',
    sectionBg: 'var(--t-section-bg)',
    sectionBorderRadius: 'var(--t-section-border-radius)',
    sectionFontColor: 'var(--t-section-font-color)',
    tabActive: 'var(--t-tab-active)',
    tabAltActive: 'var(--t-tab-alt-active)',
    tabAltInactive: 'var(--t-tab-alt-inactive)',
    tabInactive: 'var(--t-tab-inactive)',
    danger: 'var(--const-stoplight-alert)',
    greyL1: 'var( --app-gray-l1)',
    greyL2: 'var( --app-gray-l2)'
  }),
  space: (0, _lodash.default)(_presets.base.space, ['var(--app-scale-0)', 'var(--app-scale-1)', 'var(--app-scale-2)', 'var(--app-scale-3)', 'var(--app-scale-4)', 'var(--app-scale-5)', 'var(--app-scale-6)']),
  // fonts: merge(preset.fonts, []),
  // fontSizes: merge(preset.fontSizes, []),
  // fontWeights: merge(preset.fontWeights, []),
  // lineHeights: merge(preset.lineHeights, []),
  // letterSpacings: merge(preset.letterSpacings, []),
  borders: (0, _lodash.default)(_presets.base.borders, ['1px solid var(--t-secondary)']) // borderWidths: merge(preset.borderWidths, []),
  // borderStyles: merge(preset.borderStyles, []),
  // radii: merge(preset.radii, []),
  // shadows: merge(preset.shadows, []),
  // zIndices: merge(preset.zIndices, []),
  // sizes: merge(preset.sizes, [])

});

theme.colors.background = theme.colors.contentBg;
theme.colors.text = theme.colors.fontColor;
theme.colors.muted = theme.colors.primaryD1;
var _default = theme;
exports.default = _default;
//# sourceMappingURL=theme.js.map