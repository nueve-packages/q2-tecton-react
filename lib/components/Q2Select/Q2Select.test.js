"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _react2 = require("@testing-library/react");

var _Q2Select = _interopRequireDefault(require("./Q2Select"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Dropdown />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Select.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Dropdown withProps />', function () {
  it('renders  with all props correctly', function () {
    var label = 'Choose one';
    var value = '';
    var disabled = true;
    var multiple = true;
    var optional = false;
    var multilineOptions = true;
    var searchable = true;
    var children = 'Dropdown';
    var errors = ['dropdown'];

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      label: label,
      value: value,
      disabled: disabled,
      multiple: multiple,
      optional: optional,
      multilineOptions: multilineOptions,
      searchable: searchable,
      errors: errors,
      style: {
        color: 'green'
      }
    }, children)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Dropdown onClick />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      onClick: function onClick(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onClick', function () {
    var handleClick = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      onClick: handleClick
    })),
        container = _render.container;

    _react2.fireEvent.click(container.querySelector('q2-select'));

    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
describe('<Dropdown onChange />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      onChange: function onChange(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onChange', function () {
    var handleChange = jest.fn();

    var _render2 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      onChange: handleChange
    })),
        container = _render2.container;

    _react2.fireEvent.change(container.querySelector('q2-select'));

    expect(handleChange).toHaveBeenCalledTimes(0);
  });
});
describe('<Dropdown onInput />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      onInput: function onInput(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onInput', function () {
    var handleInput = jest.fn();

    var _render3 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
      onInput: handleInput
    })),
        container = _render3.container;

    _react2.fireEvent.input(container.querySelector('q2-select'));

    expect(handleInput).toHaveBeenCalledTimes(1);
  });
});
//# sourceMappingURL=Q2Select.test.js.map