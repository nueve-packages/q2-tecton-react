"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultDropdown = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonActions = require("@storybook/addon-actions");

var _addonKnobs = require("@storybook/addon-knobs");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2Select = _interopRequireDefault(require("./q2Select.png"));

var _Q2Select = _interopRequireDefault(require("./Q2Select"));

var _Q2Option = _interopRequireDefault(require("../Q2Option/Q2Option"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2Select.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2Select',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2Select.default
    }),
    jest: ['Dropdown.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
    label: (0, _addonKnobs.text)('dropdown-label', 'Choose one'),
    onChange: (0, _addonActions.action)('click'),
    value: (0, _addonKnobs.text)('dropdown-value', ''),
    disabled: (0, _addonKnobs.boolean)('dropdown-disabled', false),
    multiple: (0, _addonKnobs.boolean)('dropdown-multiple', false),
    multilineOptions: (0, _addonKnobs.boolean)('dropdown-multilineOptions', false),
    optional: (0, _addonKnobs.boolean)('dropdown-optional', false),
    onInput: (0, _addonActions.action)('Inputchange'),
    errors: (0, _addonKnobs.array)('errors', [])
  }, /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: (0, _addonKnobs.text)('option-value', '1'),
    display: (0, _addonKnobs.text)('option-display', 'Option 1'),
    disabled: (0, _addonKnobs.boolean)('option-disabled', false)
  }, "Option 1"), /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "2",
    display: "Option 2"
  }, "Option 2"));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var defaultDropdown = function defaultDropdown() {
  return /*#__PURE__*/_react.default.createElement(_Q2Select.default, {
    onInput: (0, _addonActions.action)('Inputchange')
  }, /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "1",
    display: "Option 1"
  }, "Option 1"), /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "2",
    display: "Option 2"
  }, "Option 2"), /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "3",
    display: "Option 3"
  }, "Option 3"));
};

exports.defaultDropdown = defaultDropdown;
defaultDropdown.story = {
  decorators: []
};
//# sourceMappingURL=Q2Select.stories.js.map