"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  q2Select: {}
};

var Q2Select = function Q2Select(props) {
  var errors = props.errors,
      children = props.children,
      style = props.style,
      className = props.className;
  var dropdownRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    var dropdown = dropdownRef.current || undefined;

    if (dropdown !== undefined) {
      dropdown.errors = errors;
    }
  }, [errors]);
  return /*#__PURE__*/_react.default.createElement("q2-select", (0, _extends2.default)({
    ref: dropdownRef,
    class: className
  }, props, {
    style: _objectSpread(_objectSpread({}, styles.q2Select), style)
  }), children);
};

Q2Select.propTypes = {
  label: _propTypes.default.string,
  value: _propTypes.default.string,
  selectedOptions: _propTypes.default.arrayOf(_propTypes.default.string),
  errors: _propTypes.default.arrayOf(_propTypes.default.string),
  disabled: _propTypes.default.bool,
  multiple: _propTypes.default.bool,
  searchable: _propTypes.default.bool,
  multilineOptions: _propTypes.default.bool,
  optional: _propTypes.default.bool,
  className: _propTypes.default.string,
  children: _propTypes.default.node,
  class: _propTypes.default.string,
  style: _propTypes.default.object,
  onClick: _propTypes.default.func,
  onChange: _propTypes.default.func,
  onInput: _propTypes.default.func
};
Q2Select.defaultProps = {};
var _default = Q2Select;
exports.default = _default;
//# sourceMappingURL=Q2Select.js.map