/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, { FC } from 'react';
import { DropdownChangeEvent, DropdownMouseEvent, DropdownInputEvent } from '../../types';
export interface Q2SelectProps {
    label?: string;
    value?: string;
    selectedOptions?: string[];
    errors?: string[];
    disabled?: boolean;
    multiple?: boolean;
    searchable?: boolean;
    multilineOptions?: boolean;
    optional?: boolean;
    className?: string;
    children?: React.ReactNode;
    class?: string;
    style?: object;
    onClick?: (e: DropdownMouseEvent) => void;
    onChange?: (e: DropdownChangeEvent) => void;
    onInput?: (e: DropdownInputEvent) => void;
}
declare const Q2Select: FC<Q2SelectProps>;
export default Q2Select;
