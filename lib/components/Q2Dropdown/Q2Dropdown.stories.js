"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2Dropdown = _interopRequireDefault(require("./q2Dropdown.png"));

var _Q2Dropdown = _interopRequireDefault(require("./Q2Dropdown"));

var _Q2DropdownItem = _interopRequireDefault(require("../Q2DropdownItem"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2Dropdown.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2DropDown',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2Dropdown.default
    }),
    jest: ['Q2Dropdown.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2Dropdown.default, {
    type: (0, _addonKnobs.text)('menu-type', ''),
    label: (0, _addonKnobs.text)('menu-label', 'Options'),
    icon: (0, _addonKnobs.text)('menu-icon', 'options'),
    disabled: (0, _addonKnobs.boolean)('menu-disabled', false)
  }, /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    value: (0, _addonKnobs.text)('item-type', 'option1'),
    label: (0, _addonKnobs.text)('item-label', 'Option 1'),
    disabled: (0, _addonKnobs.boolean)('item-disabled', false),
    removable: (0, _addonKnobs.boolean)('item-removable', false),
    separator: (0, _addonKnobs.boolean)('item-separator', false)
  }, (0, _addonKnobs.text)('item-label', '')), /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    value: "Option 2",
    label: "Option 2"
  }, "Option 2"));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2Dropdown.default, {
    icon: "options",
    type: "menu-type",
    label: "options"
  }, /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    label: "First",
    value: "first"
  }, "First"), /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    separator: true
  }), /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    label: "Third",
    value: "third"
  }, "Third"));
};

exports.DEFAULT = DEFAULT;
//# sourceMappingURL=Q2Dropdown.stories.js.map