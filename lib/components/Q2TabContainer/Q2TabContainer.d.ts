/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC, ReactNode } from 'react';
import { Q2TabContainer as TabContainer, TabsChangeEvent } from '../../types';
export interface Q2TabContainerProps extends TabContainer {
    value?: string;
    type?: string;
    name?: string;
    children?: ReactNode;
    className?: string;
    class?: string;
    style?: object;
    onChange?: (event: TabsChangeEvent) => void;
}
declare const Q2TabContainer: FC<Q2TabContainerProps>;
export default Q2TabContainer;
