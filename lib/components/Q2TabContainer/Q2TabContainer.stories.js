"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultQ2TabContainer = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _addonActions = require("@storybook/addon-actions");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2TabContainer = _interopRequireDefault(require("./q2TabContainer.png"));

var _Q2TabContainer = _interopRequireDefault(require("./Q2TabContainer"));

var _Q2TabPane = _interopRequireDefault(require("../Q2TabPane"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2TabContainer.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2TabContainer',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2TabContainer.default
    }),
    jest: ['Q2TabContainer.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2TabContainer.default, {
    value: (0, _addonKnobs.text)('container-value', 'tab1'),
    type: (0, _addonKnobs.select)('container-type', ['main', 'section'], 'main'),
    name: (0, _addonKnobs.text)('container-name', 'tab-group'),
    onChange: (0, _addonActions.action)('changed')
  }, /*#__PURE__*/_react.default.createElement(_Q2TabPane.default, {
    value: (0, _addonKnobs.text)('tab-value', 'tab1'),
    label: (0, _addonKnobs.text)('tab-label', 'First Tab'),
    name: (0, _addonKnobs.text)('tab-name', 'tab-group')
  }, (0, _addonKnobs.text)('tab-children', 'sample text1')));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var defaultQ2TabContainer = function defaultQ2TabContainer() {
  return /*#__PURE__*/_react.default.createElement(_Q2TabContainer.default, {
    type: "container"
  }, /*#__PURE__*/_react.default.createElement(_Q2TabPane.default, {
    value: "tab1",
    label: "First Tab",
    name: "tab-group"
  }, "tab1 sample text"), /*#__PURE__*/_react.default.createElement(_Q2TabPane.default, {
    value: "tab2",
    label: "Second Tab",
    name: "tab-group"
  }, "tab2 sample text"));
};

exports.defaultQ2TabContainer = defaultQ2TabContainer;
//# sourceMappingURL=Q2TabContainer.stories.js.map