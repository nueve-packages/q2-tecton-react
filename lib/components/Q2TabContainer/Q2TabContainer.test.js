"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@testing-library/react");

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _Q2TabContainer = _interopRequireDefault(require("./Q2TabContainer"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2TabContainer />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2TabContainer.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2TabContainer withProps />', function () {
  it('renders with all props', function () {
    var children = 'sample text';
    var value = 'tab1';
    var type = 'main';
    var name = 'tab-group';

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2TabContainer.default, {
      value: value,
      type: type,
      name: name
    }, children)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2TabContainer onChange />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2TabContainer.default, {
      onChange: function onChange(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onChange', function () {
    var handleChange = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2TabContainer.default, {
      onChange: handleChange
    })),
        container = _render.container;

    _react2.fireEvent.change(container.querySelector('q2-tab-container'));

    expect(handleChange).toHaveBeenCalledTimes(0);
  });
});
//# sourceMappingURL=Q2TabContainer.test.js.map