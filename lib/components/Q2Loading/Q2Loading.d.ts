/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC } from 'react';
import { HashMap, Q2Loading as Loading } from '../../types';
export interface Q2LoadingProps extends Loading {
    inline?: boolean;
    className?: string;
    class?: string;
    style?: HashMap;
    separate?: boolean;
}
declare const Q2Loading: FC<Q2LoadingProps>;
export default Q2Loading;
