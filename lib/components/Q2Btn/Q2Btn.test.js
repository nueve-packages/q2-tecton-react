"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _react2 = require("@testing-library/react");

var _Q2Btn = _interopRequireDefault(require("./Q2Btn"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2Btn />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Btn onClick />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onClick: function onClick(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onClick', function () {
    var handleClick = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onClick: handleClick
    })),
        container = _render.container;

    _react2.fireEvent.click(container.querySelector('q2-btn'));

    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Btn onBlur />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onBlur: function onBlur(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onBlur', function () {
    var handleBlur = jest.fn();

    var _render2 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onBlur: handleBlur
    })),
        container = _render2.container;

    _react2.fireEvent.blur(container.querySelector('q2-btn'));

    expect(handleBlur).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Btn onMouseDown />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onMouseDown: function onMouseDown(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onMouseDown', function () {
    var handleMouseDown = jest.fn();

    var _render3 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onMouseDown: handleMouseDown
    })),
        container = _render3.container;

    _react2.fireEvent.mouseDown(container.querySelector('q2-btn'));

    expect(handleMouseDown).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Btn onMouseUp />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onMouseUp: function onMouseUp(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onMouseUp', function () {
    var handleMouseUp = jest.fn();

    var _render4 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onMouseUp: handleMouseUp
    })),
        container = _render4.container;

    _react2.fireEvent.mouseUp(container.querySelector('q2-btn'));

    expect(handleMouseUp).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Btn onFocus />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onFocus: function onFocus(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onFocus', function () {
    var handleFocus = jest.fn();

    var _render5 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      onFocus: handleFocus
    })),
        container = _render5.container;

    _react2.fireEvent.focus(container.querySelector('q2-btn'));

    expect(handleFocus).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Btn withProps /> ', function () {
  it('renders with disabled prop', function () {
    var disabled = false;
    var active = false;
    var ariaLabel = '';
    var badge = false;
    var color = 'primary';
    var icon = false;
    var block = false;

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
      disabled: disabled,
      color: color,
      active: active,
      ariaLabel: ariaLabel,
      badge: badge,
      icon: icon,
      block: block
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
//# sourceMappingURL=Q2Btn.test.js.map