"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.iconQ2Btn = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _themeUi = require("theme-ui");

var _addonActions = require("@storybook/addon-actions");

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _Q2Btn = _interopRequireDefault(require("./Q2Btn"));

var _Q2Icon = _interopRequireDefault(require("../Q2Icon/Q2Icon"));

var _q2BtnPrimary = _interopRequireDefault(require("./q2BtnPrimary.png"));

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2Btn.default,
  decorators: [function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }, _storybookAddonDesigns.withDesign],
  title: 'Q2Btn',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2BtnPrimary.default
    }),
    jest: ['Q2Btn.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
    active: (0, _addonKnobs.boolean)('active', false),
    ariaLabel: (0, _addonKnobs.text)('ariaLabel', ''),
    badge: (0, _addonKnobs.boolean)('badge', false),
    color: (0, _addonKnobs.select)('color', ['primary', 'secondary'], 'primary'),
    disabled: (0, _addonKnobs.boolean)('disabled', false),
    onClick: (0, _addonActions.action)('clicked')
  }, (0, _addonKnobs.text)('children', 'Click here'));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var iconQ2Btn = function iconQ2Btn() {
  return /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
    active: (0, _addonKnobs.boolean)('active', false),
    ariaLabel: (0, _addonKnobs.text)('ariaLabel', ''),
    icon: (0, _addonKnobs.boolean)('icon', true),
    color: "",
    disabled: (0, _addonKnobs.boolean)('disabled', false),
    onClick: (0, _addonActions.action)('clicked')
  }, /*#__PURE__*/_react.default.createElement(_Q2Icon.default, {
    type: (0, _addonKnobs.select)('iconType', ['edit', 'add', 'print', 'search'], 'add')
  }));
};

exports.iconQ2Btn = iconQ2Btn;
iconQ2Btn.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2Btn.default, {
    color: "primary"
  }, "Click");
};

exports.DEFAULT = DEFAULT;
//# sourceMappingURL=Q2Btn.stories.js.map