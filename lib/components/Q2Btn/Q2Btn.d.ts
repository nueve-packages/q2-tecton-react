/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC, ReactNode } from 'react';
import { Q2Btn as button, HashMap, ButtonFocusEvent, ButtonMouseEvent } from '../../types';
export interface Q2BtnProps extends button {
    active?: boolean;
    ariaLabel?: string;
    badge?: boolean;
    block?: boolean;
    children?: ReactNode;
    class?: string;
    className?: string;
    color?: string;
    disabled?: boolean;
    icon?: boolean;
    onBlur?: (e: ButtonFocusEvent) => void;
    onClick?: (e: ButtonMouseEvent) => void;
    onFocus?: (e: ButtonFocusEvent) => void;
    onMouseDown?: (event: ButtonMouseEvent) => void;
    onMouseUp?: (event: ButtonMouseEvent) => void;
    style?: HashMap;
}
declare const Q2Btn: FC<Q2BtnProps>;
export default Q2Btn;
