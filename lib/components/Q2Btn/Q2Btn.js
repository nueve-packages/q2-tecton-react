"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  q2Btn: {
    paddingTop: '10px'
  }
};

var Q2Btn = function Q2Btn(props) {
  var children = props.children,
      style = props.style,
      className = props.className;
  var cx = (0, _classnames.default)('button', "".concat(className), "".concat(props.class));
  return /*#__PURE__*/_react.default.createElement("q2-btn", (0, _extends2.default)({}, props, {
    style: _objectSpread(_objectSpread({}, styles.q2Btn), style),
    className: cx
  }), children);
};

Q2Btn.propTypes = {
  active: _propTypes.default.bool,
  ariaLabel: _propTypes.default.string,
  badge: _propTypes.default.bool,
  block: _propTypes.default.bool,
  children: _propTypes.default.node,
  class: _propTypes.default.string,
  className: _propTypes.default.string,
  color: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  icon: _propTypes.default.bool,
  onBlur: _propTypes.default.func,
  onClick: _propTypes.default.func,
  onFocus: _propTypes.default.func,
  onMouseDown: _propTypes.default.func,
  onMouseUp: _propTypes.default.func
};
Q2Btn.defaultProps = {};
var _default = Q2Btn;
exports.default = _default;
//# sourceMappingURL=Q2Btn.js.map