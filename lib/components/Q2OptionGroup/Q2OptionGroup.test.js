"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _Q2OptionGroup = _interopRequireDefault(require("./Q2OptionGroup"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<DropdownOptionGroup />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2OptionGroup.default, {
      label: ""
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<DropdownOptionGroup withProps />', function () {
  it('renders with all props correctly', function () {
    var disabled = true;
    var children = 'DropdownOptionGroup';

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2OptionGroup.default, {
      label: "",
      disabled: disabled,
      style: {
        color: 'green'
      }
    }, children)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
//# sourceMappingURL=Q2OptionGroup.test.js.map