"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultDropdown = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _Q2Option = _interopRequireDefault(require("../Q2Option"));

var _Q2OptionGroup = _interopRequireDefault(require("./Q2OptionGroup"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2OptionGroup.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2OptionGroup',
  parameters: {
    docs: {
      page: _Readme.default
    },
    jest: ['Q2OptionGroup.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2OptionGroup.default, {
    label: (0, _addonKnobs.text)('optgroup-label', 'group'),
    disabled: (0, _addonKnobs.boolean)('option-disable', false)
  }, /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "3",
    display: "Option 3"
  }, "Option 3"));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var defaultDropdown = function defaultDropdown() {
  return /*#__PURE__*/_react.default.createElement(_Q2OptionGroup.default, {
    label: "group1"
  }, /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "4",
    display: "Option 4"
  }, "Option 4"), /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
    value: "5",
    display: "Option 5"
  }, "Option 5"));
};

exports.defaultDropdown = defaultDropdown;
defaultDropdown.story = {
  decorators: []
};
//# sourceMappingURL=Q2OptionGroup.stories.js.map