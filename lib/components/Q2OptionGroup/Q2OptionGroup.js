"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireDefault(require("react"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  q2Optgroup: {}
};

var Q2Optgroup = function Q2Optgroup(props) {
  var children = props.children,
      style = props.style;
  return /*#__PURE__*/_react.default.createElement("q2-optgroup", (0, _extends2.default)({}, props, {
    style: _objectSpread(_objectSpread({}, styles.q2Optgroup), style)
  }), children);
};

Q2Optgroup.propTypes = {
  label: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.node]),
  disabled: _propTypes.default.bool,
  className: _propTypes.default.string,
  class: _propTypes.default.string,
  style: _propTypes.default.object
};
Q2Optgroup.defaultProps = {
  label: 'optgroup',
  disabled: false,
  style: {}
};
var _default = Q2Optgroup;
exports.default = _default;
//# sourceMappingURL=Q2OptionGroup.js.map