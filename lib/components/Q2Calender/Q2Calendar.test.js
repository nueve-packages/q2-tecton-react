"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _react2 = require("@testing-library/react");

var _Q2Calendar = _interopRequireDefault(require("./Q2Calendar"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2Calendar />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Calendar  withProps/>', function () {
  var value = '';
  var label = 'Select A Date';
  var calendarLabel = 'This is calendar';
  var optional = false;
  var disabled = false;
  var disabledMsg = 'disabled';
  var disclaimer = 'test disclaimer';
  var errors = ['invalid date'];
  var displayFormat = 'DD-MM-YYYY';
  var leftAlign = false;
  var daysOfWeekChecksum = 127;
  var startDate = '';
  var endDate = '';
  it('renders with  all props correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, {
      value: value,
      label: label,
      calendarLabel: calendarLabel,
      optional: optional,
      disabled: disabled,
      disabledMsg: disabledMsg,
      disclaimer: disclaimer,
      errors: errors,
      displayFormat: displayFormat,
      leftAlign: leftAlign,
      daysOfWeekChecksum: daysOfWeekChecksum,
      startDate: startDate,
      endDate: endDate
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Calendar onClick />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, {
      onClick: function onClick(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onClick', function () {
    var handleClick = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, {
      onClick: handleClick
    })),
        container = _render.container;

    _react2.fireEvent.click(container.querySelector('q2-calendar'));

    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Calendar onChange />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, {
      onChange: function onChange(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onChange', function () {
    var handleChange = jest.fn();

    var _render2 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, {
      onChange: handleChange
    })),
        container = _render2.container;

    _react2.fireEvent.change(container.querySelector('q2-calendar'));

    expect(handleChange).toHaveBeenCalledTimes(0);
  });
});
//# sourceMappingURL=Q2Calendar.test.js.map