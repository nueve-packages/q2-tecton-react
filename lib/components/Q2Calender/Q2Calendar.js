"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  q2Calendar: {}
};

var Q2Calendar = function Q2Calendar(props) {
  var errors = props.errors,
      startDate = props.startDate,
      endDate = props.endDate,
      invalidDates = props.invalidDates,
      validDates = props.validDates,
      calendarLabel = props.calendarLabel,
      cutoffTime = props.cutoffTime,
      displayFormat = props.displayFormat,
      leftAlign = props.leftAlign,
      disabledMsg = props.disabledMsg,
      onChange = props.onChange,
      style = props.style,
      className = props.className;
  var datePickerRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    var datePicker = datePickerRef.current || undefined;

    if (datePicker !== undefined) {
      datePicker.errors = errors;
      datePicker.startDate = startDate;
      datePicker.endDate = endDate;
      datePicker.invalidDates = invalidDates;
      datePicker.validDates = validDates;
      datePicker.calendarLabel = calendarLabel;
      datePicker.cutoffTime = cutoffTime;
      datePicker.displayFormat = displayFormat;
      datePicker.leftAlign = leftAlign;
      datePicker.disabledMsg = disabledMsg;
      datePicker.onChange = onChange;
      datePicker.addEventListener('change', function () {
        return onChange;
      });
    }

    return function () {
      if (datePicker !== undefined) datePicker.removeEventListener('change', function () {
        return onChange;
      });
    };
  }, [errors, startDate, endDate, invalidDates, validDates, calendarLabel, cutoffTime, displayFormat, leftAlign, disabledMsg, onChange]);
  return /*#__PURE__*/_react.default.createElement("q2-calendar", (0, _extends2.default)({
    ref: datePickerRef,
    class: className
  }, props, {
    style: _objectSpread(_objectSpread({}, styles.q2Calendar), style)
  }));
};

Q2Calendar.propTypes = {
  value: _propTypes.default.string,
  label: _propTypes.default.string,
  calendarLabel: _propTypes.default.string,
  disclaimer: _propTypes.default.string,
  displayFormat: _propTypes.default.string,
  optional: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  disabledMsg: _propTypes.default.string,
  leftAlign: _propTypes.default.bool,
  daysOfWeekChecksum: _propTypes.default.number,
  startDate: _propTypes.default.string,
  endDate: _propTypes.default.string,
  invalidDates: _propTypes.default.arrayOf(_propTypes.default.string),
  validDates: _propTypes.default.arrayOf(_propTypes.default.string),
  cutoffTime: _propTypes.default.string,
  errors: _propTypes.default.arrayOf(_propTypes.default.string),
  className: _propTypes.default.string,
  class: _propTypes.default.string,
  style: _propTypes.default.object,
  onChange: _propTypes.default.func,
  onClick: _propTypes.default.func
};
Q2Calendar.defaultProps = {};
var _default = Q2Calendar;
exports.default = _default;
//# sourceMappingURL=Q2Calendar.js.map