"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _addonActions = require("@storybook/addon-actions");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2Calendar = _interopRequireDefault(require("./q2Calendar.png"));

var _Q2Calendar = _interopRequireDefault(require("./Q2Calendar"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2Calendar.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2Calender',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2Calendar.default
    }),
    jest: ['Q2Calendar.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, {
    value: (0, _addonKnobs.text)('value', '2020-02-12'),
    label: (0, _addonKnobs.text)('label', 'Select A Date'),
    calendarLabel: (0, _addonKnobs.text)('calendarLabel', ''),
    optional: (0, _addonKnobs.boolean)('optional', false),
    disabled: (0, _addonKnobs.boolean)('Disabled', false),
    disabledMsg: (0, _addonKnobs.text)('disabledMsg', ''),
    disclaimer: (0, _addonKnobs.text)('disclaimer', ''),
    errors: (0, _addonKnobs.array)('error', []),
    displayFormat: (0, _addonKnobs.text)('displayFormat', 'DD-MM-YYYY'),
    leftAlign: (0, _addonKnobs.boolean)('leftAlign', false),
    daysOfWeekChecksum: (0, _addonKnobs.number)('daysOfWeekChecksum', 127),
    startDate: (0, _addonKnobs.text)('startDate', '2020-02-12'),
    endDate: (0, _addonKnobs.text)('endDate', '2021-02-12'),
    onChange: (0, _addonActions.action)('changed')
  });
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2Calendar.default, null);
};

exports.DEFAULT = DEFAULT;
//# sourceMappingURL=Q2Calendar.stories.js.map