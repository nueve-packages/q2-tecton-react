/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC } from 'react';
import { CalendarChangeEvent, CalendarMouseEvent } from '../../types';
export interface Q2CalendarProps {
    value?: string;
    label?: string;
    calendarLabel?: string;
    disclaimer?: string;
    displayFormat?: string;
    optional?: boolean;
    disabled?: boolean;
    disabledMsg?: string;
    leftAlign?: boolean;
    daysOfWeekChecksum?: number;
    startDate?: string;
    endDate?: string;
    invalidDates?: string[];
    validDates?: string[];
    cutoffTime?: string;
    errors?: string[];
    className?: string;
    class?: string;
    style?: object;
    onChange?: (event: CalendarChangeEvent) => void;
    onClick?: (event: CalendarMouseEvent) => void;
}
declare const Q2Calendar: FC<Q2CalendarProps>;
export default Q2Calendar;
