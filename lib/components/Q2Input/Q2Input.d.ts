/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC } from 'react';
import { InputSyntheticEvent } from '../../types';
export interface Q2InputProps {
    label?: string;
    type?: string;
    onInput?: (e: InputSyntheticEvent) => void;
    onKeyPress?: any;
    maxlength?: string;
    placeholder?: string;
    optional?: boolean;
    disabled?: boolean;
    value?: string;
    class?: string;
    className?: string;
    clearable?: boolean;
    hints?: string[];
    errors?: string[];
    formatModifier?: string;
    iconLeft?: string;
    iconRight?: string;
    ariaLabel?: string;
    hideMessages?: boolean;
    role?: string;
    style?: object;
    id?: string;
}
declare const Q2Input: FC<Q2InputProps>;
export default Q2Input;
