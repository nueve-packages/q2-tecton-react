"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _addonActions = require("@storybook/addon-actions");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2Input = _interopRequireDefault(require("./q2Input.png"));

var _Q2Input = _interopRequireDefault(require("./Q2Input"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _q2Input.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2Input',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2Input.default
    }),
    jest: ['Input.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2Input.default, {
    label: (0, _addonKnobs.text)('label', 'Input Label'),
    onInput: (0, _addonActions.action)('oninput'),
    disabled: (0, _addonKnobs.boolean)('disabled', false),
    optional: (0, _addonKnobs.boolean)('optional', false),
    maxlength: (0, _addonKnobs.text)('maxlength', ''),
    errors: (0, _addonKnobs.array)('errors', []),
    hints: (0, _addonKnobs.array)('hints', []),
    placeholder: (0, _addonKnobs.text)('placeholder', 'Sample Placeholder'),
    type: (0, _addonKnobs.select)('type', ['text', 'email', 'currency', 'search', 'numeric', 'phone', 'number', 'tel', 'number', 'password', 'url', 'percentage', 'date', 'alphanumeric', 'alpha', 'postal', 'ssn'], 'text')
  });
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2Input.default, null);
};

exports.DEFAULT = DEFAULT;
//# sourceMappingURL=Q2Input.stories.js.map