"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  Q2Input: {}
};

var Q2Input = function Q2Input(props) {
  var errors = props.errors,
      hints = props.hints,
      label = props.label,
      value = props.value,
      placeholder = props.placeholder,
      maxlength = props.maxlength,
      type = props.type,
      optional = props.optional,
      disabled = props.disabled,
      clearable = props.clearable,
      formatModifier = props.formatModifier,
      iconLeft = props.iconLeft,
      iconRight = props.iconRight,
      ariaLabel = props.ariaLabel,
      hideMessages = props.hideMessages,
      role = props.role,
      onInput = props.onInput,
      style = props.style,
      id = props.id,
      onKeyPress = props.onKeyPress,
      className = props.className;
  var inputRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    var input = (inputRef === null || inputRef === void 0 ? void 0 : inputRef.current) || undefined;

    if (input !== undefined) {
      input.errors = errors;
      input.hints = hints;
      input.addEventListener('oninput', onInput);
    }

    return function () {
      if (input !== undefined) input.removeEventListener('oninput', onInput);
    };
  }, [errors, hints, onInput]);

  function handleChange(e) {
    var _e$nativeEvent$detail;

    props.onInput((_e$nativeEvent$detail = e.nativeEvent.detail) === null || _e$nativeEvent$detail === void 0 ? void 0 : _e$nativeEvent$detail.value);
  }

  return /*#__PURE__*/_react.default.createElement("q2-input", {
    ref: inputRef,
    label: label,
    value: value,
    placeholder: placeholder,
    onKeyPress: onKeyPress,
    maxlength: maxlength,
    type: type,
    optional: optional,
    disabled: disabled,
    clearable: clearable,
    class: className,
    hints: hints,
    errors: errors,
    "format-modifier": formatModifier,
    onInput: handleChange,
    "icon-left": iconLeft,
    "icon-right": iconRight,
    "aria-label": ariaLabel,
    hideMessages: hideMessages,
    role: role,
    style: _objectSpread(_objectSpread({}, styles.Q2Input), style),
    name: label,
    id: id
  });
};

Q2Input.propTypes = {
  label: _propTypes.default.string,
  type: _propTypes.default.string,
  onInput: _propTypes.default.func,
  onKeyPress: _propTypes.default.any,
  maxlength: _propTypes.default.string,
  placeholder: _propTypes.default.string,
  optional: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  value: _propTypes.default.string,
  class: _propTypes.default.string,
  className: _propTypes.default.string,
  clearable: _propTypes.default.bool,
  hints: _propTypes.default.arrayOf(_propTypes.default.string),
  errors: _propTypes.default.arrayOf(_propTypes.default.string),
  formatModifier: _propTypes.default.string,
  iconLeft: _propTypes.default.string,
  iconRight: _propTypes.default.string,
  ariaLabel: _propTypes.default.string,
  hideMessages: _propTypes.default.bool,
  role: _propTypes.default.string,
  style: _propTypes.default.object,
  id: _propTypes.default.string
};
Q2Input.defaultProps = {};
var _default = Q2Input;
exports.default = _default;
//# sourceMappingURL=Q2Input.js.map