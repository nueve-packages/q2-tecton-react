"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _react2 = require("@testing-library/react");

var _Q2Input = _interopRequireDefault(require("./Q2Input"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2Input />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Input.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Input withProps/>', function () {
  var errors = ['invalid'];
  var hints = ['test'];
  var label = 'text';
  var value = 'test';
  var placeholder = 'enter here';
  var maxlength = '10';
  var type = 'text';
  var optional = false;
  var disabled = false;
  var clearable = false;
  var formatModifier = '';
  var iconLeft = '';
  var iconRight = '';
  var ariaLabel = '';
  var hideMessages = false;
  var role = 'Q2Input';
  it('renders with all props', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Input.default, {
      errors: errors,
      hints: hints,
      label: label,
      value: value,
      placeholder: placeholder,
      maxlength: maxlength,
      type: type,
      optional: optional,
      disabled: disabled,
      clearable: clearable,
      formatModifier: formatModifier,
      iconLeft: iconLeft,
      iconRight: iconRight,
      ariaLabel: ariaLabel,
      hideMessages: hideMessages,
      role: role
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Input onInput />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Input.default, {
      onInput: function onInput(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onInput', function () {
    var handleInput = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Input.default, {
      onInput: handleInput
    })),
        container = _render.container;

    _react2.fireEvent.input(container.querySelector('q2-input'));

    expect(handleInput).toHaveBeenCalledTimes(1);
  });
});
//# sourceMappingURL=Q2Input.test.js.map