"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.WITH_KNOBS = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _addonActions = require("@storybook/addon-actions");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2EditableField = _interopRequireDefault(require("./q2EditableField.png"));

var _Q2EditableField = _interopRequireDefault(require("./Q2EditableField"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2EditableField.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2EditableField',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2EditableField.default
    }),
    jest: ['EditableField.test.tsx']
  }
};
exports.default = _default;

var WITH_KNOBS = function WITH_KNOBS() {
  return /*#__PURE__*/_react.default.createElement(_Q2EditableField.default, {
    onChange: (0, _addonActions.action)('changed'),
    value: (0, _addonKnobs.text)('value', 'edit'),
    label: (0, _addonKnobs.text)('label', 'Field Label'),
    editing: (0, _addonKnobs.boolean)('editing', true),
    ariaLabel: (0, _addonKnobs.text)('ariaLabel', 'Field Aria Label'),
    maxlength: (0, _addonKnobs.number)('maxlength', 10),
    persistentLabel: (0, _addonKnobs.boolean)('persistentLabel', false),
    type: (0, _addonKnobs.select)('type', ['text', 'email', 'currency', 'search', 'numeric', 'phone', 'number', 'tel', 'number', 'password', 'url', 'percentage', 'date', 'alphanumeric', 'alpha', 'postal', 'ssn'], 'text'),
    errors: (0, _addonKnobs.array)('errors', []),
    hints: (0, _addonKnobs.array)('hints', [])
  });
};

exports.WITH_KNOBS = WITH_KNOBS;
WITH_KNOBS.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2EditableField.default, null);
};

exports.DEFAULT = DEFAULT;
//# sourceMappingURL=Q2EditableField.stories.js.map