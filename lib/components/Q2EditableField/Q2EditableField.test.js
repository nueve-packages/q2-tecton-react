"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _react2 = require("@testing-library/react");

var _Q2EditableField = _interopRequireDefault(require("./Q2EditableField"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2EditableField />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2EditableField.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2EditableField withProps />', function () {
  it('renders with all props correctly', function () {
    var value = 'edit';
    var label = 'Field Label';
    var ariaLabel = 'Field Aria Label';
    var editing = true;
    var type = 'email';
    var maxlength = 10;
    var persistentLabel = true;
    var formatModifier = 'EUR';
    var hints = ['hint'];
    var errors = ['error'];

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2EditableField.default, {
      value: value,
      label: label,
      ariaLabel: ariaLabel,
      editing: editing,
      type: type,
      maxlength: maxlength,
      persistentLabel: persistentLabel,
      formatModifier: formatModifier,
      hints: hints,
      errors: errors,
      style: {
        color: 'green'
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2EditableField onChange />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2EditableField.default, {
      onChange: function onChange(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onChange', function () {
    var handleChange = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2EditableField.default, {
      onChange: handleChange
    })),
        container = _render.container;

    _react2.fireEvent.change(container.querySelector('q2-editable-field'));

    expect(handleChange).toHaveBeenCalledTimes(1);
  });
});
//# sourceMappingURL=Q2EditableField.test.js.map