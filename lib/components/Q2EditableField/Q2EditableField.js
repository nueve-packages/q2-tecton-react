"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  q2EditableField: {}
};

var Q2EditableField = function Q2EditableField(props) {
  var errors = props.errors,
      hints = props.hints,
      persistentLabel = props.persistentLabel,
      value = props.value,
      label = props.label,
      ariaLabel = props.ariaLabel,
      editing = props.editing,
      type = props.type,
      maxlength = props.maxlength,
      formatModifier = props.formatModifier,
      style = props.style,
      onChange = props.onChange;
  var editableFieldRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    var inlineEdit = editableFieldRef.current || undefined;

    if (inlineEdit !== undefined) {
      inlineEdit.errors = errors;
      inlineEdit.hints = hints;
      inlineEdit.persistentLabel = persistentLabel;
    }
  }, [errors, hints, persistentLabel]);
  (0, _react.useEffect)(function () {
    var editRef = editableFieldRef.current || undefined;

    if (editRef !== undefined) {
      editRef.addEventListener('change', onChange); // checkRef.disabled = disabled;
    }

    return function () {
      if (editRef !== undefined) editRef.removeEventListener('change', onChange);
    };
  }, [onChange]);
  return /*#__PURE__*/_react.default.createElement("q2-editable-field", {
    ref: editableFieldRef,
    value: value,
    label: label,
    ariaLabel: ariaLabel,
    editing: editing,
    type: type,
    maxlength: maxlength,
    formatModifier: formatModifier,
    errors: errors,
    persistentLabel: persistentLabel,
    style: _objectSpread(_objectSpread({}, styles.q2EditableField), style)
  });
};

Q2EditableField.propTypes = {
  value: _propTypes.default.string,
  label: _propTypes.default.string,
  ariaLabel: _propTypes.default.string,
  editing: _propTypes.default.bool,
  type: _propTypes.default.string,
  formatModifier: _propTypes.default.string,
  maxlength: _propTypes.default.number,
  hints: _propTypes.default.arrayOf(_propTypes.default.string),
  errors: _propTypes.default.arrayOf(_propTypes.default.string),
  className: _propTypes.default.string,
  persistentLabel: _propTypes.default.bool,
  class: _propTypes.default.string,
  style: _propTypes.default.object,
  onChange: _propTypes.default.func
};
Q2EditableField.defaultProps = {};
var _default = Q2EditableField;
exports.default = _default;
//# sourceMappingURL=Q2EditableField.js.map