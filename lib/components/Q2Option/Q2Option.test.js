"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _Q2Option = _interopRequireDefault(require("./Q2Option"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2Option />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
      value: "",
      display: "",
      active: false,
      selected: false
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Option withProps />', function () {
  it('renders  with all props correctly', function () {
    var value = 'Option';
    var display = 'Option';
    var disabled = true;
    var active = true;
    var selected = true;

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Option.default, {
      value: value,
      display: display,
      disabled: disabled,
      active: active,
      selected: selected,
      style: {
        color: 'green'
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
//# sourceMappingURL=Q2Option.test.js.map