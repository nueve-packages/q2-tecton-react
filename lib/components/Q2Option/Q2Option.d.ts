/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC, ReactNode } from 'react';
export interface Q2OptionProps {
    value: string;
    display: string;
    children?: string | ReactNode;
    disabled?: boolean;
    className?: string;
    class?: string;
    style?: object;
    active?: boolean;
    selected?: boolean;
}
declare const Q2Option: FC<Q2OptionProps>;
export default Q2Option;
