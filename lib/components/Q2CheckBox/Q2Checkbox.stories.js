"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _addonActions = require("@storybook/addon-actions");

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _q2Checkbox = _interopRequireDefault(require("./q2Checkbox.png"));

var _Q2Checkbox = _interopRequireDefault(require("./Q2Checkbox"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2Checkbox.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2Checkbox',
  parameters: {
    docs: {
      page: _Readme.default
    },
    design: (0, _storybookAddonDesigns.config)({
      type: 'image',
      url: _q2Checkbox.default
    }),
    jest: ['Q2Checkbox.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2Checkbox.default, {
    label: (0, _addonKnobs.text)('label', 'Option'),
    value: (0, _addonKnobs.text)('value', 'Option'),
    type: (0, _addonKnobs.select)('type', ['default', 'toggle', 'favorite'], 'default'),
    disabled: (0, _addonKnobs.boolean)('disabled', false),
    checked: (0, _addonKnobs.boolean)('checked', false),
    onChange: (0, _addonActions.action)('changed')
  }, (0, _addonKnobs.text)('children', ''));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2Checkbox.default, null);
};

exports.DEFAULT = DEFAULT;
DEFAULT.story = {
  decorators: []
};
//# sourceMappingURL=Q2Checkbox.stories.js.map