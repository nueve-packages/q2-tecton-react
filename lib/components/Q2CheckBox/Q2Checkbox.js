"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var styles = {
  q2Checkbox: {}
};

var Q2Checkbox = function Q2Checkbox(props) {
  var children = props.children,
      disabled = props.disabled,
      onChange = props.onChange,
      style = props.style,
      className = props.className;
  var checkboxRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    var checkRef = checkboxRef.current || undefined;

    if (checkRef !== undefined) {
      checkRef.addEventListener('change', onChange);
      checkRef.disabled = disabled;
    }

    return function () {
      if (checkRef !== undefined) checkRef.removeEventListener('change', onChange);
    };
  }, [onChange, disabled]);
  return /*#__PURE__*/_react.default.createElement("q2-checkbox", (0, _extends2.default)({
    ref: checkboxRef,
    class: className
  }, props, {
    style: _objectSpread(_objectSpread({}, styles.q2Checkbox), style)
  }), children); // return <img src={checkbox} alt="checkbox" />;
};

Q2Checkbox.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.node, _propTypes.default.string]),
  checked: _propTypes.default.bool,
  value: _propTypes.default.string,
  label: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  type: _propTypes.default.string,
  onChange: _propTypes.default.func,
  className: _propTypes.default.string,
  class: _propTypes.default.string,
  style: _propTypes.default.object,
  alignment: _propTypes.default.string,
  ariaLabel: _propTypes.default.string,
  indeterminate: _propTypes.default.bool
};
Q2Checkbox.defaultProps = {};
var _default = Q2Checkbox;
exports.default = _default;
//# sourceMappingURL=Q2Checkbox.js.map