"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@testing-library/react");

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _Q2Checkbox = _interopRequireDefault(require("./Q2Checkbox"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2Checkbox />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Checkbox.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<Q2Checkbox onChange />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Checkbox.default, {
      onChange: function onChange(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onChange', function () {
    var handleChange = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Checkbox.default, {
      onChange: handleChange
    })),
        container = _render.container;

    _react2.fireEvent.change(container.querySelector('q2-checkbox'));

    expect(handleChange).toHaveBeenCalledTimes(1);
  });
});
describe('<Q2Checkbox withProps /> ', function () {
  it('renders with all props correctly', function () {
    var label = 'Option';
    var value = 'Option';
    var type = 'default';
    var disabled = false;
    var checked = false;

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Checkbox.default, {
      label: label,
      value: value,
      type: type,
      disabled: disabled,
      checked: checked
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
//# sourceMappingURL=Q2Checkbox.test.js.map