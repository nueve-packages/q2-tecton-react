"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultRadioButtons = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonActions = require("@storybook/addon-actions");

var _themeUi = require("theme-ui");

var _addonKnobs = require("@storybook/addon-knobs");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _Q2Radio = _interopRequireDefault(require("../Q2Radio/Q2Radio"));

var _Q2RadioGroup = _interopRequireDefault(require("./Q2RadioGroup"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2RadioGroup.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2RadioButtonGroup',
  parameters: {
    docs: {
      page: _Readme.default
    },
    jest: ['RadioButtonGroup.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2RadioGroup.default, {
    label: (0, _addonKnobs.text)('radiogroup-label', 'Radio group'),
    name: (0, _addonKnobs.text)('radiogroup-name', 'icecreamFlavor'),
    value: (0, _addonKnobs.text)('radiogroup-value', 'radiogroup'),
    disabled: (0, _addonKnobs.boolean)('radiogroup-disabled', false),
    onChange: (0, _addonActions.action)('changed')
  }, /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
    label: (0, _addonKnobs.text)('radiobutton-label', 'Option 2'),
    value: (0, _addonKnobs.text)('radiobutton-value', 'option 2'),
    ariaLabel: (0, _addonKnobs.text)('radiobutton-ariaLabel', 'radio button'),
    disabled: (0, _addonKnobs.boolean)('radiobutton-disabled', false)
  }), /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
    label: "Option 3",
    value: "option 3",
    ariaLabel: "radio button",
    disabled: false
  }));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var defaultRadioButtons = function defaultRadioButtons() {
  return /*#__PURE__*/_react.default.createElement(_Q2RadioGroup.default, null, /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
    label: "Option 1",
    value: "1"
  }), /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
    label: "Option 2",
    value: "2"
  }), /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
    label: "Option 3",
    value: "3"
  }));
};

exports.defaultRadioButtons = defaultRadioButtons;
//# sourceMappingURL=Q2RadioGroup.stories.js.map