"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT = exports.containKnobs = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _storybookAddonDesigns = require("storybook-addon-designs");

var _addonKnobs = require("@storybook/addon-knobs");

var _themeUi = require("theme-ui");

var _Readme = _interopRequireDefault(require("./Readme.mdx"));

var _Q2DropdownItem = _interopRequireDefault(require("./Q2DropdownItem"));

var _theme = _interopRequireDefault(require("../../theme"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _default = {
  component: _Q2DropdownItem.default,
  decorators: [_storybookAddonDesigns.withDesign, function (storyFn) {
    return /*#__PURE__*/_react.default.createElement(_themeUi.ThemeProvider, {
      theme: _theme.default
    }, storyFn());
  }],
  title: 'Q2DropDownItem',
  parameters: {
    docs: {
      page: _Readme.default
    },
    jest: ['Q2DropdownItem.test.tsx']
  }
};
exports.default = _default;

var containKnobs = function containKnobs() {
  return /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    value: (0, _addonKnobs.text)('item-type', 'option1'),
    label: (0, _addonKnobs.text)('item-label', 'Option 1'),
    disabled: (0, _addonKnobs.boolean)('item-disabled', false),
    removable: (0, _addonKnobs.boolean)('item-removable', false),
    separator: (0, _addonKnobs.boolean)('item-separator', false)
  }, (0, _addonKnobs.text)('item-label', ''));
};

exports.containKnobs = containKnobs;
containKnobs.story = {
  decorators: [_addonKnobs.withKnobs]
};

var DEFAULT = function DEFAULT() {
  return /*#__PURE__*/_react.default.createElement(_Q2DropdownItem.default, {
    label: "First",
    value: "first"
  }, "First");
};

exports.DEFAULT = DEFAULT;
//# sourceMappingURL=Q2DropdownItem.stories.js.map