/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC, ReactNode } from 'react';
export interface Q2DropdownItemProps {
    disabled?: boolean;
    value?: string;
    label?: string;
    removable?: boolean;
    separator?: boolean;
    children?: ReactNode;
    className?: string;
    class?: string;
    style?: object;
}
declare const Q2DropdownItem: FC<Q2DropdownItemProps>;
export default Q2DropdownItem;
