/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FC } from 'react';
import { Q2Radio as Radio, RadioButtonChangeEvent, RadioButtonFormEvent, RadioButtonMouseEvent } from '../../types';
export interface Q2RadioButtonProps extends Radio {
    label?: string;
    value?: string;
    disabled?: boolean;
    ariaLabel?: string;
    className?: string;
    class?: string;
    style?: object;
    onBlur?: (event: RadioButtonChangeEvent) => void;
    onFocus?: (event: RadioButtonFormEvent) => void;
    onClick?: (event: RadioButtonMouseEvent) => void;
    onChange?: (event: RadioButtonChangeEvent) => void;
}
declare const Q2Radio: FC<Q2RadioButtonProps>;
export default Q2Radio;
