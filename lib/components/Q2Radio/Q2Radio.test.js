"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/web.url.to-json");

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@testing-library/react");

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var _Q2Radio = _interopRequireDefault(require("./Q2Radio"));

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('<Q2Radio />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, null)).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<RadioButton withProps />', function () {
  it('renders with all props correctly', function () {
    var label = 'option1';
    var value = 'option1';
    var disabled = true;
    var ariaLabel = 'radioButton';

    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      label: label,
      value: value,
      disabled: disabled,
      ariaLabel: ariaLabel,
      style: {
        color: 'red'
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
describe('<RadioButton onClick />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      onClick: function onClick(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onClick', function () {
    var handleClick = jest.fn();

    var _render = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      onClick: handleClick
    })),
        container = _render.container;

    _react2.fireEvent.click(container.querySelector('q2-radio'));

    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
describe('<RadioButton onBlur />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      onBlur: function onBlur(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onBlur', function () {
    var handleBlur = jest.fn();

    var _render2 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      onBlur: handleBlur
    })),
        container = _render2.container;

    _react2.fireEvent.blur(container.querySelector('q2-radio'));

    expect(handleBlur).toHaveBeenCalledTimes(1);
  });
});
describe('<RadioButton onFocus />', function () {
  it('renders correctly', function () {
    var tree = _reactTestRenderer.default.create( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      onFocus: function onFocus(f) {
        return f;
      }
    })).toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('handles onFocus', function () {
    var handleFocus = jest.fn();

    var _render3 = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_Q2Radio.default, {
      onFocus: handleFocus
    })),
        container = _render3.container;

    _react2.fireEvent.focus(container.querySelector('q2-radio'));

    expect(handleFocus).toHaveBeenCalledTimes(1);
  });
});
//# sourceMappingURL=Q2Radio.test.js.map