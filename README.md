# q2-tecton-react

[![GitHub stars](https://img.shields.io/github/stars/nuevesolutions/q2-tecton-react.svg?style=social&label=Stars)](https://github.com/nuevesolutions/q2-tecton-react)

> react wrapper around q2 tecton web components

Please ★ this repo if you found it useful ★ ★ ★

## Demo

See a [demo](https://nuevesolutions.com)

## Installation

```sh
npm install --save q2-tecton-react
```

## Dependencies

- [NodeJS](https://nodejs.org)

## Usage

[Contribute](https://github.com/nuevesolutions/q2-tecton-react/blob/master/CONTRIBUTING.md) usage docs

## Support

Submit an [issue](https://github.com/nuevesolutions/q2-tecton-react/issues/new)

## Screenshots

[Contribute](https://github.com/nuevesolutions/q2-tecton-react/blob/master/CONTRIBUTING.md) a screenshot

## Contributing

Review the [guidelines for contributing](https://github.com/nuevesolutions/q2-tecton-react/blob/master/CONTRIBUTING.md)

## License

[Apache-2.0 License](https://github.com/nuevesolutions/q2-tecton-react/blob/master/LICENSE)

[Nueve Solutions LLC](https://nuevesolutions.com) © 2020

## Changelog

Review the [changelog](https://github.com/nuevesolutions/q2-tecton-react/blob/master/CHANGELOG.md)

## Credits

- [Nueve Solutions LLC](https://nuevesolutions.com) - Author

## Support on Liberapay

A ridiculous amount of coffee ☕ ☕ ☕ was consumed in the process of building this project.

[Add some fuel](https://liberapay.com/nuevesolutions/donate) if you'd like to keep me going!

[![Liberapay receiving](https://img.shields.io/liberapay/receives/nuevesolutions.svg?style=flat-square)](https://liberapay.com/nuevesolutions/donate)
[![Liberapay patrons](https://img.shields.io/liberapay/patrons/nuevesolutions.svg?style=flat-square)](https://liberapay.com/nuevesolutions/donate)
